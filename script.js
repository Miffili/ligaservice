(function scraper() {
  // DOM elements
  const championshipInput = document.querySelector("#championship");
  const groupInput = document.querySelector("#group");
  const tableClassInput = document.querySelector("#tableClass");
  const hideColInput = document.querySelector("#hideCol");
  const btn = document.querySelector("#go");
  const resetBtn = document.querySelector("#reset");
  const resultDisplay = document.querySelector("#result");

  // START SCRAPER
  const baseURL =
    "https://hvmv-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBDE.woa/wa/groupPage?";

  function getData() {
    reset();

    const tableClass = tableClassInput.value;
    const hideCol = hideColInput.value;
    // Build request URL
    const championshipComponent = encodeURIComponent(
      championshipInput.value
    ).replace("%20", "+");

    const championshipURL = `${baseURL}championship=${championshipComponent}`;

    const targetURL = `${championshipURL}&group=${groupInput.value}`;

    // Fetch URL content
    fetch(targetURL).then(data => {
      // Request OK?
      if (data.status === 200) {
        // if (!(data.status >= 400 && data.status <= 599)) {
        data.text().then(text => {
          //  Parse HTML document to DOM
          const parser = new DOMParser();
          const doc = parser.parseFromString(text, "text/html");

          // Find target table
          let table = doc.querySelector(".result-set:first-of-type");

          // Remove all empty columns
          const tableHeads = table.querySelectorAll("th");
          tableHeads.forEach((th, i) => {
            let _isColumnEmpty = true;
            if (th.innerHTML.replace("&nbsp;", "").trim() !== "") {
              _isColumnEmpty = false;
            }
            const cells = table.querySelectorAll("tr td:nth-child(" + (i+1) + ")");
            cells.forEach(cell => {
              if (cell.innerHTML.replace("&nbsp;", "").trim() !== "") {
                _isColumnEmpty = false;
              }
            });
            if (_isColumnEmpty) {
              th.remove();
              cells.forEach(cell => {
                cell.remove();
              })
            }
          });

          // Remove links in table
          const nowraps = table.querySelectorAll("[nowrap]");
          nowraps.forEach(el => {
            const link = el.querySelector("a");
            const team = link.innerHTML;
            el.innerHTML = team;
            link.remove();
          });

          // if optional parameters
          // add class to table tag
          if (tableClass.length) {
            table.classList.add(tableClass);
            resultDisplay.insertAdjacentHTML(
              "afterbegin",
              `<h3>Table with class: ${tableClass}</h3>`
            );
          }
          // hide columns
          if (hideCol) {
            const rows = table.querySelectorAll("tr");
            rows.forEach(el => {
              const items = Array.from(el.children);
              items.forEach((el, i) => {
                if (hideCol.includes(i + 1)) {
                  el.classList.add("hide");
                }
              });
            });
          }

          // Return table markup
          resultDisplay.insertAdjacentElement("beforeend", table);
        });
      }
    });
  }
  // END SCRAPER

  function reset() {
    if (resultDisplay.children.length) {
      if (resultDisplay.querySelector("h3")) {
        resultDisplay.querySelector("h3").remove();
      }
      resultDisplay.querySelector("table").remove();
    }
  }

  btn.addEventListener("click", getData);
  resetBtn.addEventListener("click", reset);
})();

// POLYFILLs
// from:https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/remove()/remove().md
(function(arr) {
  arr.forEach(function(item) {
    if (item.hasOwnProperty("remove")) {
      return;
    }
    Object.defineProperty(item, "remove", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        if (this.parentNode !== null) this.parentNode.removeChild(this);
      }
    });
  });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function(callback, thisArg) {
    thisArg = thisArg || window;
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}
